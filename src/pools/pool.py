from __future__ import annotations

from threading import Lock
from time import sleep
from typing import Callable, Generic, Optional, TypeVar

_T = TypeVar("_T")
_O = TypeVar("_O")


class Resource(Generic[_T]):
    def __init__(self, factory: Callable[[], _T]):
        self._value: Optional[_T] = None
        self.factory = factory
        self.claimed = False

    @property
    def value(self) -> _T:
        if self._value is None:
            self._value = self.factory()

        return self._value


class Pool(Generic[_T]):
    def __init__(self, count: int, factory: Callable[[], _T]):
        self.resources: list[Resource[_T]] = [Resource(factory) for _ in range(count)]
        self.lock = Lock()

    def __enter__(self) -> _T:
        pass

    def __exit__(self) -> None:
        pass

    def do(self, fn: Callable[[_T], _O]) -> _O:
        """Run function fn when a resource is available."""

        with self as resource:
            return fn(resource)
